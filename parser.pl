#! /usr/bin/perl

use strict;
use warnings;
use Tie::File;

my (@src, $src); #Source
my $des = "out.txt";
my $line_num = 0;
my $full_header;
my $partial_header;
my $next_line;
my $text;
my $parsed_header;

tie (@src,'Tie::File',"Questcards2015.txt") or die; # make array out of file.
open(DES,'>',$des) or die; 

foreach $src (@src) { # Iterate through the array
    $text = $src; 
    $next_line = @src[($line_num + 1)]; # Peek at next line
    if ($text =~ m/^(QUESTCARD [\dA-Z]+)( .*)?$/) { # Parse header, save into vars
	$full_header = $2;
	$partial_header = $1;
	$parsed_header = $partial_header;
	$parsed_header =~ s/ /_/g; # Replace spaces with _ so it can be used as a link
	print DES "<span id=\"$parsed_header\">'''$partial_header'''</span> $full_header\n"; # Header as span id, allows it to be linked too.
	print DES "\n";
    } elsif ($next_line =~ m/^(?:QUESTCARD [\dA-Z]+)(?: .*)?$/) { # Checks if the next line is a header if so, add back to top link.
	print DES "\n";
	print DES "[[#TOP|Back to top]]\n";
	print DES "\n";
    } elsif ($text =~ m/^TREASURE HUNTS\s*$/) { # Checks for start of treasuer hunt table, adds wiki syntax.
	print DES "{| class=\"wikitable sortable\"\n!style=\"text-align:left;\"| Letter\n!Treasure Hunt\n!Location\n|-\n";
    } elsif ($text =~ m/^ZONE QUESTS\s*$/) { # same as above but for quest table.
	print DES "{| class=\"wikitable sortable\"\n!style=\"text-align:left;\"| Number\n!Quest\n!Location\n!Difficulty\n|-\n"
    } elsif ($text =~ s/^(\d+)\s+(\w.*\w!?)\s+([A-Z]+)\s+(.*)\s*$/| $1 || [[#QUESTCARD_$1|$2]] || $3 || $4\n/) { #makes tables
	print DES $text;
	if ($next_line !~ m/^\d+\s+\w.*\w!?\s+[A-Z]+\s+.*$/) { # Is the next line a table row? If not end the table.
	    print DES "|}\n";
	} else {
	    print DES "|-\n";
	}
    } elsif ($text =~ s/^([A-Z])\s+(\w.*\w!?)\s+([A-Z]+)\s*$/| $1 || [[#QUESTCARD_$1|$2]] || $3\n/) { #makes tables
	print DES $text;
	if ($next_line !~ m/^[A-Z]\s+\w.*\w!?\s+[A-Z]+$/) {
	    print DES "|}\n";
	} else {
	    print DES "|-\n";
	}
    } elsif ($text =~ m/--+/) {
	print DES "";
    } elsif ($text =~ s/^$/\n/) {
	print DES $text;
    } elsif ($text =~ s/^- (.*)$/* $1\n/) { # Make bullet list.
	print DES $text;
    } elsif ($text =~ s/^\d[\.\)] (.*)$/# $1\n/) { # Make numbered list.
	print DES $text;
    } else {
	$text =~ s/^(.*)$/$1<br \/>\n/; #default case, prints text with newline and <br /> for line breaks on the wiki.
	print DES $text;
    }
    $line_num = ($line_num + 1) # Keeps track of the line we're on.
}

untie(@src);
close(DES);
